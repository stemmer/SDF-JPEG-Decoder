#!/usr/bin/env bash


SOURCEPATH="./JPEG-Decoder"
INSTALLPATH="./mb"
BUILDPATH="./build"
INCLUDEPATHS="-I$SOURCEPATH"
SHARED_CFLAGS="-O3 -c -mcpu=v9.6 -mlittle-endian -fdata-sections -ffunction-sections -std=c99"
GCC=mb-gcc
OBJCOPY=mb-objcopy
AR=mb-ar

for lib in libjpeg.a libjpeg-ef.a libjpeg-ea.a
do
    if [ -f "$INSTALLPATH/$lib" ] ; then
        echo -e "\e[1;31m$lib does already exist."
        echo -e "\e[1;30mTo create a new $lib library, the old one must be removed manually."
        exit 1
    fi
done

if [ -d "$BUILDPATH" ] ; then
    echo -e "\e[1;31m$BUILDPATH does already exist."
    echo -e "\e[1;30mThe scripts expects that this directory does not exist. Please remove it manually."
    exit 1
fi

mkdir -p $BUILDPATH

# 1st argument: libname "jpeg-ef" for example
# 2nd argument: CFLAGS
function Build
{
    LIBNAME="$1"
    CFLAGS="$2"

    SOURCES=$(find $SOURCEPATH -type f -name "*.c")
    for s in $SOURCES ; 
    do
        echo -e -n "\e[1;34m - $s\e[0m "
        FILENAME="${s##*/}"
        OBJFILE="${FILENAME%.*}.o"
        $GCC $CFLAGS $INCLUDEPATHS -I$SOURCEPATH -c -o "$BUILDPATH/$OBJFILE" $s
        if [ $? -ne 0 ] ; then
            echo -e "\e[1;31m$GCC FAILED!\e[0m"
            exit 1
        else
            echo -e "\e[1;32m✔\e[0m"
        fi
    done

    $AR rv $INSTALLPATH/lib${LIBNAME}.a $BUILDPATH/*.o
    rm $BUILDPATH/*.o
}

mkdir -p $INSTALLPATH

# Build Libraries
Build jpeg    "$SHARED_CFLAGS -mxl-soft-mul    -mxl-soft-div    -msoft-float"
Build jpeg-ef "$SHARED_CFLAGS -mxl-soft-mul    -mxl-soft-div    -mhard-float -mxl-float-convert -mxl-float-sqrt"
Build jpeg-ea "$SHARED_CFLAGS -mno-xl-soft-mul -mno-xl-soft-div -msoft-float"

# Copy Headers
cp $SOURCEPATH/*.h $INSTALLPATH/.

# Make Clean
rmdir $BUILDPATH

