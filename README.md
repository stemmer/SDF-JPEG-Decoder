# SDF JPEG-Decoder

A Bare-Metal JPEG Decoder implementation following the SDF Model of Computation. It features decoding the 3 color channels concurrently and is separated in 8 Actors.